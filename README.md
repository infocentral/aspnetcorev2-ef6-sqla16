Example asp.net core v2 (full framework) application using EF6 talking to SQL Anywhere v16 database.

Check out:
 - app.config
 - Controllers\SampleController.cs

To do this to your project:

 - You should use a nuget package to install SQL Anywhere 16 assemblies.
 - You must `Install-Package System.Configuration.ConfigurationManager`
 - You must add `app.config` to your project and set it to copy to output directory.  No, `web.config` will not work.


A couple of notes regarding this sample:

 - The nuget package `InfoCentral.SqlAnywhere16.EF6.Net` is a private package that simply contains the SQL Anywhere v16 assemblies needed for EF6.
 - The sample connects to a table EmailLogs (Which is a legacy table, hence the column mappings).
 - It's a terrible design. Don't do what this sample does.  Use DI to at least inject the DbContext.


