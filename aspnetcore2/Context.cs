﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace aspnetcore2
{
    public class Email
    {
        public int Id { get; set; }
        public string To { get { return MailTo; } }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        protected string MailTo { get; set; }
    }

    public class Context : DbContext
    {
        public Context(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            Database.SetInitializer<Context>(null);
        }

        public DbSet<Email> Emails { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            var order = modelBuilder.Entity<Email>();
            order.Property(x => x.Id)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
                .HasColumnName("EntryEmailLog");
            //order.Property(x => x.MailTo).HasColumnName("MailTo");
            order.Property(x => x.Cc).HasColumnName("MailCc");
            order.Property(x => x.Bcc).HasColumnName("MailBcc");
            order.Property(x => x.From).HasColumnName("MailFrom");
            order.Property(x => x.Subject).HasColumnName("MailSubject");
            order.Property(x => x.Body).HasColumnName("MailBody");
            order.ToTable("EmailLogs", "infobase");
            order.HasKey(x => x.Id);
        }


    }
}
