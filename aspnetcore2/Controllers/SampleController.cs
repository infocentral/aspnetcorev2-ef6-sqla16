﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace aspnetcore2.Controllers
{
    [Route("api/[controller]")]
    public class SampleController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            using (var context = new Context("eng=sampledb;dbn=sampledb;UID=secure;PWD=password"))
            {
                var email = await context.Emails.Where(x => x.Id == 253).SingleOrDefaultAsync();

                return Ok(email);
            }
        }
    }
}
